import 'package:flutter/material.dart';

Color primaryColor = Color.fromARGB(255, 71, 3, 111);
Color accentColor = Color.fromARGB(255, 173, 102, 212);
Color selectedColor = Color.fromARGB(255, 255, 213, 108);
Color white = Colors.white;
Color transparent = Colors.transparent;
